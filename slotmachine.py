# -*- coding: utf-8 -*-
"""
Created on Mon Dec 11 08:05:11 2017

@author: enovi
"""
##License: GPL 3.0

##slot machine game
import random
start_balance = 100

def main():
    choice = 'y'
    print('***Welcome to Slot Machine!***')
    print('Place your bet and pull the lever!')
    while choice == 'y':               
        choice = input('New game y/n? ').lower()
        if choice == 'y':
            print ('Starting new game')
            slots()
        elif choice == 'n':
            return
        else:
            print('Enter a valid choice')
            choice = 'y'

def slots():
    global start_balance
    balance = 0
    bet = 0
    a = 0
    b = 0
    c = 0
    d = ""
    e = ""
    f = ""
    print("Welcome to Slot Machine")
    start_balance = float(input("Select starting balance, maximum 100: "))
    if start_balance > 100:
        start_balance = 100 
    balance = start_balance
    while balance > 0:
        print("Current Balance is: ",balance)
        decision = input("Would you like to make a new bet? Enter y or n: ").lower()
        if decision == "y":
            bet = float(input("Place your bet: "))
            if bet <= balance and bet > 0:
                balance = balance - bet
            else:
                print("Invalid bet")
                bet = 0
            a = random.randint(1,12)
            b = random.randint(1,12)
            c = random.randint(1,12)
            ##map numbers to symbols here
            if a == 1:
                d = "!!!!"
            if b == 1:
                e = "!!!!"
            if c == 1:
                f = "!!!!"
            ##continue for next 11 symbols
            if a == 2:
                d = "@@@@"
            if b == 2:
                e = "@@@@"
            if c == 2:
                f = "@@@@"
            if a == 3:
                d = "####"
            if b == 3:
                e = "####"
            if c == 3:
                f = "####"
            if a == 4:
                d = "$$$$"
            if b == 4:
                e = "$$$$"
            if c == 4:
                f = "$$$$"    
            if a == 5:
                d = "%%%%"
            if b == 5:
                e = "%%%%"
            if c == 5:
                f = "%%%%" 
            if a == 6:
                d = "^^^^"
            if b == 6:
                e = "^^^^"
            if c == 6:
                f = "^^^^"
            if a == 7:
                d = "****"
            if b == 7:
                e = "****"
            if c == 7:
                f = "****"
            if a == 8:
                d = "****"
            if b == 8:
                e = "****"
            if c == 8:
                f = "****"
            if a == 9:
                d = "(((("
            if b == 9:
                e = "(((("
            if c == 9:
                f = "(((("
            if a == 10:
                d = "))))"
            if b == 10:
                e = "))))"
            if c == 10:
                f = "))))"
            if a == 11:
                d = "----"
            if b == 11:
                e = "----"
            if c == 11:
                f = "----"
            if a == 12:
                d = "++++"
            if b == 12:
                e = "++++"
            if c == 12:
                f = "++++"    
            #print symbols
            print(d,e,f)
            #jackpot logic
            if (a == 4 and a == b == c):
                print("4x win!")
                balance = balance + (bet * 4)
            if (a == 8 and a == b == c):
                print("8x win!")
                balance = balance + (bet * 8)
            if (a == 12 and a == b == c):
                print("12x win!")
                balance = balance + (bet * 12)
            #smaller prize
            elif (a == 4 or a == 8 or a == 12):
                print("2x win!")
                balance = balance + (bet * 2)                   
        elif decision == "n":
            if start_balance > balance:
                print("You lost money! ")
                return
            if start_balance == balance:
                print("You broke even! ")
                return
            if start_balance < balance:
                print("You gained money! ")
                return
        else:
            print('Choose y or n.')
            decision = 'y'
    if balance == 0:
        print("Out of money! ")
        return
main()                
